FROM mcr.microsoft.com/azure-cli

COPY pipe /
RUN chmod a+x /*.sh

ENTRYPOINT ["/pipe.sh"]
